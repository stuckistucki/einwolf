<?php

function einwolf_scripts()
{
    wp_enqueue_style('einwolf-style', get_stylesheet_uri());
}
add_action('wp_enqueue_scripts', 'einwolf_scripts');

/**
 * Disables all kinds of crap which is enabled by default
 */
function disable_lotsofcrap()
{
    // disable emoji crap
    remove_action('wp_head', 'print_emoji_detection_script', 7);
    remove_action('admin_print_scripts', 'print_emoji_detection_script');
    remove_action('wp_print_styles', 'print_emoji_styles');
    remove_action('admin_print_styles', 'print_emoji_styles');

    // disable wp embed crap
    wp_deregister_script('wp-embed');

    // remove rsd link from head
    remove_action('wp_head', 'rsd_link');

    // disable XML-RPC API
    add_filter('xmlrpc_enabled', '__return_false');

    // remove wlw manifest from head
    remove_action('wp_head', 'wlwmanifest_link');

    // remove generator meta tag from head
    remove_action('wp_head', 'wp_generator');

    // remove REST API info from head and headers (does not actually disable the API!)
    remove_action('xmlrpc_rsd_apis', 'rest_output_rsd');
    remove_action('wp_head', 'rest_output_link_wp_head', 10);
    remove_action('template_redirect', 'rest_output_link_header', 11);
}
add_action('init', 'disable_lotsofcrap');

// disables gallery inline css crap
add_filter('use_default_gallery_style', '__return_false');

function customFormatGallery($string, $attr)
{
    // $attr:
    //   'size' => string 'full' (length=4)
    //   'link' => string 'none' (length=4)
    //   'columns' => string '1' (length=1)
    //   'ids' => string '79,75' (length=5)
    //   'orderby' => string 'post__in' (length=8)
    //   'include' => string '79,75' (length=5)

    $imgSize = $attr['columns'] > 1 ? array(600, 400, false) : array(1200, 800, false);

    $output = "<div class=\"gallery\" data-columns=\"".$attr['columns']."\">";
    $posts = get_posts(array('include' => $attr['ids'],'post_type' => 'attachment'));

    foreach ($posts as $imagePost) {
        $output.= "<div class=\"gallery-item\">";
        $output.= '<img';
        $output.= ' src="'.ipq_get_theme_image_url($imagePost->ID, $imgSize).'"';
        $output.= '>';
        $output .= "</div>";
    }

    $output .= "</div>";

    return $output;
}
add_filter('post_gallery', 'customFormatGallery', 10, 2);


/**
 * Register our sidebars and widgetized areas.
 *
 */
function einwolf_widgets_init()
{
    register_sidebar(array(
        'name'          => 'Kontakt',
        'id'            => 'contact',
        'before_widget' => '<div>',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="rounded">',
        'after_title'   => '</h2>',
    ));
}
add_action('widgets_init', 'einwolf_widgets_init');
