<!doctype html>
<html class="no-js">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <title><?php bloginfo('name'); ?> | <?php if (is_home()) : echo bloginfo('description'); endif; ?><?php wp_title('', true); ?></title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <div id="content">
