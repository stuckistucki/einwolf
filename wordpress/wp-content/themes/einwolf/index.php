<?php get_header(); ?>
<header class="main">
<?php if (is_active_sidebar('contact')) : ?>
      <aside class="contact">
            <?php dynamic_sidebar('contact'); ?>
      </aside>
<?php endif; ?>
</header>

<?php if (have_posts()) :
   while (have_posts()) :
      the_post();?>
      <article class="post">
            <header>
                  <h2><?php the_title(); ?></h2>
            </header>
            <?php the_content(); ?>
   </article>
   <?php endwhile;
endif;

get_footer();
